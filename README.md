# Docker Data Only Container Demo

## Usage

git clone https://bitbucket.org/seankibler/docker-data-only-containers

docker build -t [name] .

docker create -v /demodata --name demodata [name] /bin/true

docker run -it --volumes-from demodata --name demo1 [name] /bin/bash

docker run -it --volumes-from demodata --name demo2 [name] /bin/bash

Inside demo1 run:
touch /demodata/manatee.txt

Inside demo2 run:
ls /demodata

Inside demo2 run:
rm -f /demodata/manatee.txt

Inside demo1 run:
ls /demodata

Such simple, much success.

## License

[WTFPL](http://www.wtfpl.net/)
